# Design

View and edit family trees.

competes: gramps, myheritage

interacts: browser client, git hosting

constitutes: 

includes: 

resembles:

patterns: 

target audiences: genealogist, family record keeper

stakeholders: fetsorn, Sjoerd van der Hoorn

edit family trees in browser

https://www.gedcom.org/samples.html

competitors run on servers or desktop, cache GEDCOM in SQL, add custom metadata and functionality, then provide GEDCOM export. genea caches GEDCOM in memory, allows user to view and edit family tree, and returns a GEDCOM file. genea can also store GEDCOM on git and publish to git hosting providers.

- [X] fix a bug, add "0 TRLR" correctly at the end of GEDCOM export
- [X] add tag "_UID" to gedcom export, to add unique IDs for individuals and families
- [X] add flake.nix for easier hosting of genea
- [X] fix issue #8 https://github.com/genea-app/genea-app/issues/8 в https://github.com/genea-app/genea-app/pull/10
- document genea code that draws graphs fram gedcom
