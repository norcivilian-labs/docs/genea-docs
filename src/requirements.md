# Requirements

 - view family tree
 - edit family tree
 - export valid GEDCOM 5.5 file
 - import valid GEDCOM 5.5 file
 - view local part of the family tree
 - view entire family tree
 - add picture to individual
