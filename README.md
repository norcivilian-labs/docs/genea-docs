# genea-docs

Documentation for the [genea](https://gitlab.com/norcivilian-labs/genea) project, built with [mdBook](https://github.com/rust-lang/mdBook) and hosted at [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).
